var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('pages/header2.hbs', { title: 'Express' });
});



router.get('/fig', function(req, res) {
  res.render('pages/figure.hbs', { title: 'Express' });
});



router.get('/ask', function(req, res) {
  res.render('pages/register.hbs', { title: 'Express' });
});

module.exports = router;
